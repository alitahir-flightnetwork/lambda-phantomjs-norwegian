# README #

### What is this repository for? ###

* This repository contains an example of how to work with phantomjs in node-lambda environment. It makes a request to norwegian air and grabs the outbound table as text as an example.
* The deployed version can be accessed via the following URL
* https://5cz4sx8zje.execute-api.us-east-1.amazonaws.com/Norwegian/?adults=1&destination=AMS&dateOut=2017-03-05&children=0&dateIn=2017-03-24&origin=OSL&roundTrip=true&infants=0

### How to get started? ###

* git pull
* npm install

### How to deploy to AWS ###

* compress all files including node_modules/ and phantomjs_linux-x86_64