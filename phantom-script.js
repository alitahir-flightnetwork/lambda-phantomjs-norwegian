var system = require('system');
var page = require('webpage').create();
var args = system.args;

// Example of how to get arguments passed from node script
// args[0] would be this file's name: phantom-script.js
var unusedArg = args[1];

var params = JSON.parse(unusedArg);

// Send some info node's childProcess' stdout

var dateIn = params.dateIn.split('-');
var dateOut = params.dateOut.split('-');


page.open("https://www.norwegian.com/us/booking/flight-tickets/select-flight/" +
    "?D_City=" + params.origin +
    "&A_City=" + params.destination +
    "&D_Day=" + dateIn[2] +
    "&D_Month=" + dateIn[0] + dateIn[1] +
    "&D_SelectedDay=" + + dateIn[2] +
    "&R_Day=" + dateOut[2] +
    "&R_Month=" + dateOut[0] + dateOut[1] +
    "&R_SelectedDay=" + dateOut[2] +
    "&AgreementCodeFK=-1" +
    "&CurrencyCode=USD" +
    "&ShowNoFlights=True", function (status) {
    system.stdout.write('opened site? ' + status);
    var outbound = page.evaluate(function () {
        return document.getElementsByClassName("avadayoutbound")[0];
    });

    system.stdout.write('------- OUTBOUND --------');
    system.stdout.write(outbound.innerText.trim());
    phantom.exit();
});

//system.stdout.write('hello from phantom!');


