var childProcess = require('child_process');
var path = require('path');

exports.handler = function(event, context, callback) {
	
	// Set the path as described here: https://aws.amazon.com/blogs/compute/running-executables-in-aws-lambda/
	process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
	
	// Set the path to the phantomjs binary
	var phantomPath = path.join(__dirname, 'phantomjs_linux-x86_64');

	console.log('----event-----');
	console.log(event);
	if (!event.params) {
		event.params = {querystring:{"roundTrip":"true","dateIn":"2017-03-05","infants":"0","children":"0","adults":"1","origin":"OSL","dateOut":"2017-03-24","destination":"AMS"}};
	}

    if (event.params.querystring) {
        console.log('----event.params.querystring-----');
        console.log(event.params.querystring);

        var params = event.params.querystring;

        // Arguments for the phantom script
        var processArgs = [
            path.join(__dirname, 'phantom-script.js'),
            JSON.stringify(params)
        ];

        // Launch the child process
        childProcess.execFile(phantomPath, processArgs, function(error, stdout, stderr) {
            if (error) {
                context.fail(error);
                return;
            }
            if (stderr) {
                context.fail(error);
                return;
            }
            context.succeed(stdout);

            callback(stdout);
        });
	}
};